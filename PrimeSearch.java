import java.util.*;

public class PrimeSearch {

	/* parse the input and call functions accordingly
	*/
	public static void main(String[] args) {

		if ((args.length != 1) && (args.length != 2)) {
			printHelp(false);

		} else if (args.length == 1) {

			if (args[0].equalsIgnoreCase("help")) {
				printHelp(true);
			}

			else {

				// no method is invoked -> default to isPrime
				try {
					int n = Integer.valueOf(args[0]);

					if (n > 0) System.out.println(isPrime(n));

					else printHelp(false);
				}

				catch (Exception e) {
					printHelp(false);
				}
			}

		// user invoked a method
		} else {

			int n;

			try {
				n = Integer.valueOf(args[1]);

				if (args[0].equalsIgnoreCase("help")) {
					printHelp(true);

				} else if (n <= 0) {
					printHelp(false);

				} else if (args[0].equalsIgnoreCase("isPrime")) {
					System.out.println(isPrime(n));

				} else if (args[0].equalsIgnoreCase("NthPrime") || args[0].equalsIgnoreCase("findPrime")) {
					System.out.println(findPrime(n));

				} else if ((args[0].equalsIgnoreCase("sequence")) || (args[0].equalsIgnoreCase("trialDivision"))) {
					printList(trialDivision(n));

				} else if (args[0].equalsIgnoreCase("upTo") || (args[0].equalsIgnoreCase("sieve"))) {
					printList(sieve(n));

				// user wrote a bad method name
				} else {
					printHelp(false);
				}

			// user wrote a bad argument
			} catch (Exception e) {
				printHelp(false);
			}
		}
	}


	/* Print usage instructions. Called with false argument if the user enters
	bad input. Called with true argument if the user types "help"
	*/
	public static void printHelp(boolean verbose) {

		if (verbose) {
			System.out.println("\nHOW TO USE:\n\n> java PrimeSearch <methodName> n"
			+ "\n\n- n must be a positive integer"
			+ "\n- method name is not case sensitive"
			+ "\n- defaults to isPrime if no method name is specified"
			+ "\n\nMETHODS:"
			+ "\n- isPrime - check whether n is a prime number or not"
			+ "\n- NthPrime (or findPrime) - get the n:th prime number in the sequence of prime nubmers"
			+ "\n- sequence (or trialDivision) - get the sequence of prime numbers up to the n:th prime"
			+ "\n- upTo (or sieve) - get a list of the primes that are less than or equal to n"
			+ "\n- help - print this message\n");

		} else {
			System.out.println("bad input\n\nFOR INSTRUCTIONS ON HOW TO USE:\n>java PrimeSearch help");
		}
	}


	public static void printList(List<Integer> primes) {

		int delimiter = primes.size() - 1;

		// I would use a for-each loop, but then it would print a comma after the last element as well
		for (int i = 0; i <= delimiter; i++) {

			System.out.print(primes.get(i));
			if (i < delimiter) System.out.print(", ");
		}

		System.out.println();
	}


	/* Return true if input is prime, false if not. Default method; gets called
	if the user only inputs a number
	*/
	public static boolean isPrime(int x) {

		return sieve(x).contains(x);
	}


	/* Return the n:th prime
	*/
	public static int findPrime(int n) {

		List<Integer> primeList = trialDivision(n);
		return primeList.get(primeList.size() - 1);
	}


	/* Trial divison algorithm to find a list of the first n prime numbers
	*/
	public static List<Integer> trialDivision(int n) {

		List<Integer> primes = new ArrayList<Integer>();
		primes.add(2);

		int delimiter;
		boolean divisible;

		for (int i = 3; primes.size() < n; i++) {

			divisible = false;
			delimiter = (int)Math.sqrt(i);
			// doing the trial division only up to the square root of the integer to be factored
			// cuts down on computing time drastically without affecting the result

			for (int divisor : primes) {
				// it is enough to only factor with prime numbers less than the integer to be
				// factored, rather than all integers less than it

				if (divisor > delimiter) break;

				if (i % divisor == 0) {

					divisible = true;
					break;  // saving more time
				}
			}

			if (!divisible) primes.add(i);
		}

		return primes;
	}


	/* Return a list of all prime numbers that are less than or equal to the
	natural number n.

	An implementation of the ancient algorithm for finding prime numbers, sieve
	of Erastothenes.

	The same results could also be achieved by a trial division where the
	for-loop would evaluate the iterator rather than the number of found prime
	numbers, but I wanted to implement this algorithm out of interest.
	*/
	public static List<Integer> sieve(int n) {

		List<Integer> set = new ArrayList<Integer> ();

		for (int i = 2; i <= n; i++) {
			set.add(i);
		}

		int delimiter = (int)Math.sqrt(n);
		int divisor;
		int j;

		for (int i = 0; i < set.size(); i++) {
			// the conditional i < set.size() doesn't really matter since the loop should never run
			// for that long anyways

			divisor = set.get(i);
			j = i + 1;

			if (divisor > delimiter) break;

			while (j < set.size()) {
				// can't use a for-loop since we don't want to iterate j if an element is removed,
				// lest we skip an element

				if (set.get(j) % divisor == 0) set.remove(j);

				else j++;
			}
		}

		return set;
	}
}
