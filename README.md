# JavaPrimeSearch

A command line tool written in Java that finds prime numbers.
Has functionality for:
- checking if a number is prime or not
- generating the n:th prime number
- generating a list of n prime numbers
- generating all prime numbers less than or equal to n

Calculating prime numbers takes time. If you for example want to calculate
all prime numbers smaller than or equal to a million, prepare to wait a few
minutes.

This program does not consider 1 to be a prime number.


## How to use
Compile:

You must have Java installed

Open your terminal in the directory where PrimeSearch.java resides

Type the following command

\> javac primesearch.java

Instructions on how to use:

Type the following command

\> java PrimeSearch help

## Known Issues
According to wikipedia the sieve of Erastothenes algorithm should be faster
than trial division for numbers less than 10 000 000. However this
implementation of it runs slower than the trial division. Clearly there is room
for optimization. Perhaps I will get to it one day.
